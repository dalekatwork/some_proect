import React, { useState } from "react";
import { Form, Input, Button, Row, Col, message } from "antd";
import { formItemLayout, sleep, tailFormItemLayout } from "./common";

const regex = /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[^\w\s]).{8,}$/;

function MainInformation() {
  const [form] = Form.useForm();
  const [loading, setLoading] = useState(false);

  async function onFinish() {
    setLoading(true);
    message.loading("Saving additional information", 0);
    await sleep(500);
    message.destroy();
    setLoading(false);
    message.success("Additional information saved successfully");
  }

  return (
    <Form
      {...formItemLayout}
      form={form}
      name="register"
      onFinish={onFinish}
      initialValues={{
        residence: ["zhejiang", "hangzhou", "xihu"],
        prefix: "86",
      }}
      scrollToFirstError
    >
      <Row>
        <Col span={12} style={{ paddingRight: 16 }}>
          <Form.Item
            name="email"
            label="E-mail"
            rules={[
              {
                type: "email",
                message: "The input is not valid E-mail!",
              },
              {
                required: true,
                message: "Please input your E-mail!",
              },
            ]}
          >
            <Input disabled={loading} />
          </Form.Item>
        </Col>
        <Col span={12} style={{ paddingLeft: 16 }}>
          <Form.Item
            name="password"
            label="Password"
            rules={[
              {
                required: true,
                message: "Please input your password!",
              },
              () => ({
                validator(rule, value) {
                  if (!value || regex.test(value)) {
                    return Promise.resolve();
                  }

                  return Promise.reject(
                    "Password must contain uppercase letters, lowercase letters, numbers and special characters, and should be at least 8 characters long"
                  );
                },
              }),
            ]}
            hasFeedback
          >
            <Input.Password disabled={loading} />
          </Form.Item>
        </Col>
      </Row>
      <Row>
        <Col span="12" style={{ paddingRight: 16 }}>
          <Form.Item
            name="confirm"
            label="Confirm Password"
            dependencies={["password"]}
            hasFeedback
            rules={[
              {
                required: true,
                message: "Please confirm your password!",
              },
              ({ getFieldValue }) => ({
                validator(rule, value) {
                  if (!value || getFieldValue("password") === value) {
                    return Promise.resolve();
                  }

                  return Promise.reject(
                    "The two passwords that you entered do not match!"
                  );
                },
              }),
            ]}
          >
            <Input.Password disabled={loading} />
          </Form.Item>
        </Col>
      </Row>

      <Form.Item {...tailFormItemLayout}>
        <Button type="primary" loading={loading} htmlType="submit">
          Register
        </Button>
      </Form.Item>
    </Form>
  );
}

export default MainInformation;
