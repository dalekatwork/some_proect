export const formItemLayout = {
  labelCol: {
    span: 24,
  },
  wrapperCol: {
    span: 24,
  },
};
export const tailFormItemLayout = {
  wrapperCol: {
    xs: {
      span: 4,
      offset: 0,
    },
    sm: {
      span: 4,
      offset: 0,
    },
  },
};

export function sleep(ms: number) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}
