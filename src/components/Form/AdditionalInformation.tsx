import React, { useState } from "react";
import { Form, Input, Button, Row, Col, Select, message } from "antd";
import { formItemLayout, sleep, tailFormItemLayout } from "./common";

const { Option } = Select;

function AdditionalInformation() {
  const [form] = Form.useForm();
  const [loading, setLoading] = useState(false);

  async function onFinish() {
    setLoading(true);
    message.loading("Saving additional information", 0);
    await sleep(500);
    message.destroy();
    setLoading(false);
    message.success("Additional information saved successfully");
  }

  return (
    <Form
      {...formItemLayout}
      form={form}
      name="register"
      onFinish={onFinish}
      initialValues={{
        residence: ["zhejiang", "hangzhou", "xihu"],
        prefix: "86",
      }}
      scrollToFirstError
    >
      <Row>
        <Col span={12} style={{ paddingRight: 16 }}>
          <Form.Item
            name="firstname"
            label="First Name"
            rules={[{ required: true }]}
          >
            <Input disabled={loading} />
          </Form.Item>
        </Col>
        <Col span={12} style={{ paddingLeft: 16 }}>
          <Form.Item
            name="lastname"
            label="Last Name"
            rules={[{ required: true }]}
          >
            <Input disabled={loading} />
          </Form.Item>
        </Col>
      </Row>
      <Row>
        <Col span="12" style={{ paddingRight: 16 }}>
          <Form.Item
            name="address"
            label="Address"
            rules={[{ required: true }]}
          >
            <Input
              disabled={loading}
              placeholder="Street, House number, Postal code"
            />
          </Form.Item>
        </Col>
        <Col span="12" style={{ paddingLeft: 16 }}>
          <Form.Item
            name="country"
            label="Country"
            rules={[{ required: true }]}
          >
            <Select placeholder="Select a county" allowClear disabled={loading}>
              <Option value="germany">Germany</Option>
              <Option value="austrailia">Austrailia</Option>
              <Option value="switzerland">Switzerland</Option>
            </Select>
          </Form.Item>
        </Col>
      </Row>
      <Form.Item {...tailFormItemLayout}>
        <Button type="primary" htmlType="submit" loading={loading}>
          Register
        </Button>
      </Form.Item>
    </Form>
  );
}

export default AdditionalInformation;
