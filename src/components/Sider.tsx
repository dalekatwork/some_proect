import React from "react";
import { Layout, Menu } from "antd";
import {
  SettingOutlined,
  AreaChartOutlined,
  ReadOutlined,
  HomeOutlined,
  UploadOutlined,
  AccountBookOutlined,
} from "@ant-design/icons";

const { Sider } = Layout;

export default function ({ collapse }: { collapse: boolean }) {
  return (
    <Sider
      trigger={null}
      collapsible
      collapsed={collapse}
      style={{ height: "100vh" }}
    >
      <div className="logo" />
      <Menu theme="dark" mode="inline" defaultSelectedKeys={["1"]}>
        <Menu.Item key="1" icon={<HomeOutlined />}>
          Home
        </Menu.Item>
        <Menu.Item key="2" icon={<AccountBookOutlined />}>
          My Account
        </Menu.Item>
        <Menu.Item key="3" icon={<UploadOutlined />}>
          My Company
        </Menu.Item>
        <Menu.Item key="4" icon={<SettingOutlined />}>
          My Settings
        </Menu.Item>
        <Menu.Item key="5" icon={<ReadOutlined />}>
          News
        </Menu.Item>
        <Menu.Item key="6" icon={<AreaChartOutlined />}>
          Analytics
        </Menu.Item>
      </Menu>
    </Sider>
  );
}
