import React, { useState } from "react";
import { Layout, Tabs } from "antd";
import { MenuUnfoldOutlined, MenuFoldOutlined } from "@ant-design/icons";
import "./App.css";
import Sider from "./components/Sider";
import MainInformation from "./components/Form/MainInformation";
import AdditionalInformation from "./components/Form/AdditionalInformation";

const { TabPane } = Tabs;

const { Header, Content } = Layout;

function App() {
  const [collapse, setCollapse] = useState(false);

  function toggle() {
    setCollapse(!collapse);
  }

  function callback(key: string) {
    console.log(key);
  }

  return (
    <Layout>
      <Sider collapse={collapse} />
      <Layout className="site-layout">
        <Header className="site-layout-background" style={{ padding: 0 }}>
          {React.createElement(
            collapse ? MenuUnfoldOutlined : MenuFoldOutlined,
            {
              className: "trigger",
              onClick: toggle,
            }
          )}
        </Header>
        <Content
          className="site-layout-background"
          style={{
            margin: "24px 16px",
            padding: 24,
            minHeight: 280,
          }}
        >
          <Tabs defaultActiveKey="1" onChange={callback}>
            <TabPane tab="Main Information" key="1">
              <MainInformation />
            </TabPane>
            <TabPane tab="Additional Information" key="2">
              <AdditionalInformation />
            </TabPane>
          </Tabs>
        </Content>
      </Layout>
    </Layout>
  );
}

export default App;
