# Innoloft exercise

This implementation uses typescript, husky linter and antd library to implement the forms.
To mimic an ajax call, I added sleep timers.

Here are the steps to run this

    cd innoloft_frontend_application
    yarn && yarn start
